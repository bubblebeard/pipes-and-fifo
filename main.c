#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include <signal.h>
#include <string.h>

//#define PATH "/home/bubblebeard/NetBeansProjects/Pipes/mine"
#define TAKE 5
#define LOOP 1

int writer(int);
int reader(int, int, int*, pid_t*);

int main(int argc, char** argv) {

    if (argc < 2) {
        printf("enter count of orcs\n");
        return (EXIT_FAILURE);
    }
    int orcs = atoi(argv[1]);
    int fd[orcs];
    pid_t pid[orcs];
    int gold = 50;
    pid_t childpid;

    for (int i = 0; i < orcs; i++) {
        int pipe1[2];
        pipe(pipe1);

        if ((childpid = fork()) == -1) {
            perror("fork");
            exit(1);
        }

        if (childpid == 0) {
            close(pipe1[0]);
            writer(pipe1[1]);
            exit(0);
        } else {
            close(pipe1[1]);
            fd[i] = pipe1[0];
            pid[i] = childpid;
        }
    }
    reader(orcs, gold, fd, pid);

    return (0);
}

int writer(int fd) {
    char string[] = "REQUEST\n";
    while (LOOP) {
        write(fd, string, (strlen(string) + 1));
        sleep(rand() % 5);
    }
    return 1;
}

int reader(int orcs, int gold, int *fd, pid_t *pid) {
    int cur = 0;
    char readbuffer[80];
    
    while (LOOP) {
        if (gold <= 0) {
            printf("no gold left\n");
            kill(0, SIGKILL);
            return (EXIT_SUCCESS);
        }
        
        if (read(fd[cur], readbuffer, sizeof (readbuffer)) > 0) {
            printf("Received message %s", readbuffer);
            memset(readbuffer, 0, strlen(readbuffer));
            if (gold < TAKE) {
                printf("orc%d takes %d gold. 0 gold left\n", pid[cur], gold);
                gold = 0;
            } else {
                printf("orc%d takes %d gold. %d gold left\n", pid[cur], TAKE, gold - TAKE);
                gold = gold - TAKE;
            }
            if (cur == orcs - 1) {
                cur = 0;
            } else {
                cur++;
            }
        } else {
            if (cur == orcs - 1) {
                cur = 0;
            } else {
                cur++;
            }
        }
    }
}
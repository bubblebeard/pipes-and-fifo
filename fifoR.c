#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>
#include <signal.h>
#include <string.h>
#include <sys/stat.h> 
#include <wait.h> 
#include <fcntl.h> 
#include <errno.h>

//#define PATH "/home/bubblebeard/NetBeansProjects/Pipes/mine"
#define TAKE 5
#define LOOP 1

int writer(int);
int reader(int, int, int*, pid_t*);

int main(int argc, char** argv) {

    if (argc < 2) {
        printf("enter count of orcs\n");
        return (EXIT_FAILURE);
    }
    int orcs = atoi(argv[1]);
    int fdfifo[orcs];
    pid_t pid[orcs];
    int gold = 50;
    pid_t childpid;
    char myfifo[256];
    
    for (int i = 0; i < orcs; i++) {
        memset(myfifo,0,strlen(myfifo));
        sprintf(myfifo,"/tmp/myfifo_%d",i);
        mkfifo(myfifo,0666);
        
        if ((childpid = fork()) == -1) {
            perror("fork");
            exit(1);
        }

        if (childpid == 0) {
            int fd = open(myfifo,O_WRONLY,0);
            writer(fd);
            exit(0);
        } else {
            int fd = open(myfifo,O_RDONLY,0);
            fdfifo[i] = fd;
            pid[i] = childpid;
        }
    }
    reader(orcs, gold, fdfifo, pid);

    return (0);
}

int writer(int fd) {
    char string[] = "REQUEST\n";
    while (LOOP) {
        write(fd, string, (strlen(string) + 1));
        sleep(rand() % 5);
    }
    return 1;
}

int reader(int orcs, int gold, int *fd, pid_t *pid) {
    int cur = 0;
    char readbuffer[80];
    
    while (LOOP) {
        if (gold <= 0) {
            printf("no gold left\n");
            kill(0, SIGKILL);
            return (EXIT_SUCCESS);
        }
        
        if (read(fd[cur], readbuffer, sizeof (readbuffer)) > 0) {
            printf("Received message %s", readbuffer);
            memset(readbuffer, 0, strlen(readbuffer));
            if (gold < TAKE) {
                printf("orc%d takes %d gold. 0 gold left\n", pid[cur], gold);
                gold = 0;
            } else {
                printf("orc%d takes %d gold. %d gold left\n", pid[cur], TAKE, gold - TAKE);
                gold = gold - TAKE;
            }
            if (cur == orcs - 1) {
                cur = 0;
            } else {
                cur++;
            }
        } else {
            if (cur == orcs - 1) {
                cur = 0;
            } else {
                cur++;
            }
        }
    }
}